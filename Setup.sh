#! /bin/bash
# this is the main Setup file for the final obc (work in progress)

# allow pi to shutdown
sudo echo "pi ALL = (root) NOPASSWD: /sbin/shutdown" >> /etc/sudoers

sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install git -y
sudo apt-get install python3 -y
sudo apt-get install python3-pip -y
sudo apt-get install python3-rpi.gpio -y

#i2c tools
sudo apt install i2c-tools

# Camera
sudo apt-get install python3-picamera -y

# ADC adc1115
sudo pip3 install adafruit-ads1x15

# Numpy
sudo pip3 install numpy

sudo apt-get install python3-smbus -y
#sudo pip3 install smbus
sudo apt-get install -y python-rpi.gpio python3-rpi.gpio python3-watchdog

#to get the actual git version:
#git clone https://PusteblumeKSat@bitbucket.org/ksatstuttgart/papellobcsoftware.git

# setup necessary commands
echo "creating mount point..."
sudo mkdir /home/pi/mntData

echo "copy piData_1..."
sudo cp commands/piData_1 /usr/bin/
sudo chmod +x /usr/bin/piData_1

echo "copy piData_2..."
sudo cp commands/piData_2 /usr/bin/
sudo chmod +x /usr/bin/piData_2

echo "copy reloadModules_1..."
sudo cp commands/reloadModules_1 /usr/bin/
sudo chmod +x /usr/bin/reloadModules_1

echo "copy reloadModules_2..."
sudo cp commands/reloadModules_2 /usr/bin/
sudo chmod +x /usr/bin/reloadModules_2

echo "copy swapData..."
sudo cp commands/swapData /usr/bin/
sudo chmod +x /usr/bin/swapData
