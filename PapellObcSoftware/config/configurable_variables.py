'''##################################################
# PAPELL
# first created by Moritz Sauer 1/28/2018
# modified by chris
# modified by Martin
# modified by Jan-Erik 2/27/2018
#
# successfully tested (did you really test an empty file :D )
#
# overview of all variable used and modifiable
#
# DO NOT write back into this file!
#
##################################################'''

######
# Papell is allowed to work with FerroFluid
# this variable has to be True to allow the Valves to open.
# This is ONLY allowed on the ISS !!!!!!!
######

SECURITY_VALVES = True

######
# PSU
######
PSU_CONSTANTS_REVISION = 4
PSU_LOGGING_FRAMERATE = 2

PSU_CHECKING_FRAMERATE = 0.5
PSU_CHECKING_prio2 = 5
PSU_CHECKING_prio3 = 20  ## can't be lower than 11



BAT = [80, 100, True, False]

BAT0_TEMP_THRESHOLD = 80
BAT0_TEMP_CRITICAL = 100
BAT0_WORKING_FLAG = True
BAT0_DEAD_FLAG = False
BAT0 = [BAT0_TEMP_THRESHOLD, BAT0_TEMP_CRITICAL, BAT0_WORKING_FLAG, BAT0_DEAD_FLAG]

BAT1_TEMP_THRESHOLD = 80
BAT1_TEMP_CRITICAL = 100
BAT1_WORKING_FLAG = True
BAT1_DEAD_FLAG = False
BAT1 = [BAT0_TEMP_THRESHOLD, BAT0_TEMP_CRITICAL, BAT0_WORKING_FLAG, BAT0_DEAD_FLAG]

BAT2_TEMP_THRESHOLD = 80
BAT2_TEMP_CRITICAL = 100
BAT2_WORKING_FLAG = True
BAT2_DEAD_FLAG = False
BAT2 = [BAT0_TEMP_THRESHOLD, BAT0_TEMP_CRITICAL, BAT0_WORKING_FLAG, BAT0_DEAD_FLAG]

BAT3_TEMP_THRESHOLD = 80
BAT3_TEMP_CRITICAL = 100
BAT3_WORKING_FLAG = True
BAT3_DEAD_FLAG = False
BAT3 = [BAT0_TEMP_THRESHOLD, BAT0_TEMP_CRITICAL, BAT0_WORKING_FLAG, BAT0_DEAD_FLAG]

BAT4_TEMP_THRESHOLD = 80
BAT4_TEMP_CRITICAL = 100
BAT4_WORKING_FLAG = True
BAT4_DEAD_FLAG = False
BAT4 = [BAT0_TEMP_THRESHOLD, BAT0_TEMP_CRITICAL, BAT0_WORKING_FLAG, BAT0_DEAD_FLAG]

BAT_STATS = {
    "BAT0": BAT0,
    "BAT1": BAT1,
    "BAT2": BAT2,
    "BAT3": BAT3,
    "BAT4": BAT4

}


## validity parameters
#####################
SOC_LIMITS = [0, 100]
VOLTAGE_LIMITS = [3000, 4400]
#maybe separate for charging?
CAPACITY_LIMITS = [200, 1400]
FULL_CAPACITY_LIMITS = [900, 1400]
TEMP_LIMITS = [20, 100]
CURRENT_LIMITS = [-5000, 5000]
POWER_LIMITS = [VOLTAGE_LIMITS[1]*CURRENT_LIMITS[0], VOLTAGE_LIMITS[1]*CURRENT_LIMITS[1]]

# ToDo: THIS DATA NEEDS TO BE DETERMINED!!!! first value is warning, second is error
UVP_VOLTAGE_LIMITS = [3000, 2900]  # this will trigger safemode, should never be used cause of SOC limits
OVP_VOLTAGE_LIMITS = [4300, 4500]  # this will disconnect battery
OVC_CURRENT_LIMITS = [1000,  1200]  # this will disconnect battery
SOC_CHECK_LIMITS = [30, 20]        # this will trigger safemode and hopefully charging?!


CHARGING_MAX_VOLTAGE_DIFF = [200]  # this will prevent connecting of batteries

############
# Safe State
############
TEMP_MIN = 10
TEMP_MAX = 60

######
# Actuator
######

ENABLE_VALVES = False

ENABLE_PUMP = False

ENABLE_INJECT = False

######
# Experimental Areas
######

ENABLE_EA1 = False

ENABLE_EA2 = False

######
# Sensors
######

ENABLE_MGM_EA1 = False

ENABLE_MGM_EA2 = False

ENABLE_TEMP_FUELGAUGE = False

ENABLE_TEMP_OBC = False

ENABLE_SOUND = False

ENABLE_ACC = False

ENABLE_CURRENT_ACTBOARD = False

ENABLE_CURRENT_EA1 = False

ENABLE_CURRENT_EA2 = False

ENABLE_FUELGAUGE = False

######
# Cameras
######

ENABLE_CAM_EA1 = False

ENABLE_CAM_EA2 = False

ENABLE_CAM_PR = False

######
#
######


######
