##################################################
# PAPELL
# first created by Robin Schweigert
# modified by --
# successfully tested 04.02.2018 by Robin Schweigert
#
# The Logger implements all logging abilities of the
# project. It adds a timestamp, a severity level and
# the sender in addition to the message. This
# combination is then printed on the console and
# stored to a file. Every time a the Software
# restarts a new file is created.
#
##################################################
import datetime
import time
import os
import fnmatch

from helpers.Singleton import Singleton


@Singleton
class Logger:
    #path_logs = "logs/"  # path for all Logger files

    def __init__(self):
        """
        controls for the log directory and creates it
        creates the save files
        """
        self.path_logs = "logs/"
        if not os.path.exists("logs/"):
            os.makedirs("logs/")
            self.log(LogLevel.INFO, "Logger", "Created new log directory")
        self.log_file = open("logs/" + self.new_beginning(".log", self.path_logs ) + self.get_current_time_str() + ".log", "a")

    def log(self, log_level, sender, msg):
        """
        if not verbose, the message is printed in the console
        :param sender: The sender of the message.
        :param log_level: One of the following levels: INFO, VERBOSE, EXCEPTION, WARNING, ERROR.
        :param msg: Message to be saved as string.
        """
        if log_level != "Verbose":
            temp_msg = "[" + str(log_level) + "][" + str(sender) + "][" + str(
                Logger.get_instance().get_current_time_str()) + "]: " + str(msg)
            print(temp_msg)
            try:
                self.log_file.write(temp_msg + "\n")
                self.log_file.flush()
            except Exception as e:
                print(str(e))

        else:
            temp_msg = "[" + str(log_level) + "][" + str(sender) + "][" + str(
                Logger.get_instance().get_current_time_str()) + "]: " + str(msg)
            print(temp_msg)
            try:
                self.log_file.write(temp_msg + "\n")
                self.log_file.flush()
            except Exception as e:
                print(str(e))


    def get_current_time_millis(self):
        """
        :return: Returns the time in milliseconds since the epoch as an Integer
        """
        return int(round(time.time() * 1000))

    def get_current_time_str(self):
        '''
        :return: Returns the current date and time as [yyyy-mm-dd hh:mm:ss]
        '''
        return str(datetime.datetime.now()).split(".")[0].replace(" ", "_").replace(":", "_")

    def new_beginning(self, ending: str, path:str):
        """
        this method returns a new first number fur any files pattern and dirrectory. if missing number, a 0 is returned
        :param ending: ".log" ending or type or pattern of file name that needs to get a number
        :param path: "logs/" directory with the filenames, that will be searched
        :return: int of the new file number. enlarged by one.
        """
        result = []
        file_name_list = self.findilein( ending, path)
        if file_name_list is False:
            return str(0)+"_"
        for name in file_name_list:
            stringi = name.split("_")
            try:
                new_num = int(stringi[0]) + 1
                result.append(new_num)

            except Exception as e:
                result.append(0)
                pass
        return str(max(result))+"_"

    def findilein(self, pattern: str, path: str):
        """
        finds you all filenames in path with pattern in the name and returns them as list with strings
        :param pattern: part of the filename to search for. example: ".log"
        :param path: example: "logs/"
        :return: [ "name" "name" ] list with all names with the pattern in str
        """
        result = []
        #print(path, pattern)
        for root, dirs, files in os.walk(path):
            for name in files:
                if pattern in name:
                    result.append(name)
        if not result:
            # print(result)
            return False
        # print("return fing: ", result)
        return result

    def check_running_number(self):
        n_old = 2
        folder = "lolo/"
        file_name = "numbel.txt"
        try:
            if os.path.isdir(folder):  # check for file
                n_old = int(n_old)
                f_new = open(file_name)
                n_new = f_new.readline()
                if int(n_old):
                    print("yes")
                if n_new == n_old:
                    n_new = n_new +1
        except Exception:
            pass

    def define_running_number(self):
        pass



class LogLevel:
    """
    Possible default LogLevels, can be replaced by any string message
    """
    # all different log levels
    ERROR = "Error"
    WARNING = "Warning"
    EXCEPTION = "Exception"
    INFO = "Info"
    VERBOSE = "Verbose"


if __name__ == "__main__":

    # Test für neue Namensgebung:

    lolo = Logger()
    b = lolo.new_beginning(".log", "state_manager/logs")
    print("print new beginning: ", b)

