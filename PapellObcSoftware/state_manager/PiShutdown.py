##################################################
# PAPELL
# first created by Maximilian von Arnim
# modified by Robin Schweigert
# successfully tested 04.02.2018 by Robin Schweigert
#
# Pi Shutdown implements the surveillance of the
# power connection. It signals the state manager
# to transit into the SaveTheAstronauteState and
# starts an internal countdown as failsafe.
#
##################################################
import RPi.GPIO as GPIO
import time
import os

from state_manager.Logger import Logger, LogLevel


class PiShutdown:

    __usb_pin_no = 42  # pls change TODO
    
    def __init__(self, usb_pin_no, state_manager):
        """
        sets the pin to read the USB condition
        """
        self.__usb_pin_no = usb_pin_no
        self._state_manager = state_manager

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.__usb_pin_no, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        # create testing variable
        self.__running = True

    def shutdowncheck(self):
        """
        continuously checking the power connection at 100Hz
        """
        while self.__running:
            if not GPIO.input(self.__usb_pin_no):
                time.sleep(3)
                if not GPIO.input(self.__usb_pin_no):
                    Logger.get_instance().log(LogLevel.WARNING, "PiShutdown", "Power disconnect recognized.")
                    self._state_manager.save_the_astronaut_sig.value = True
                    time.sleep(70)
                    os.system("/usr/bin/sudo /sbin/shutdown -h 0")
                    return
            time.sleep(0.01)
