##################################################
# PAPELL
# first created by Robin Schweigert
# modified by Martin (23.3.2018)
# successfully tested Martin (23.3.2018)
#
# The StateManager is the central routine for the
# overall experiment. It manages the the whole
# procedure and starts all necessary additional
# threads.
#
##################################################
import os
import time
from multiprocessing import Process, Value
import Main
from helpers.ChangeListener import ChangeListener


from helpers.PiCom import PiCom

# Imports for IdleState
from helpers.ConfigRead import ConfigReader

# Imports for Operations
from camera.cameras import Camera

from state_manager.Logger import Logger, LogLevel


class StateManager:
    framerate = 2

    IDLESTATE = 0
    SAFESTATE = 1
    SAVETHEASTRONAUT = 2
    OPERATIONSTATE = 3

    safe_state_sig = Value('b', False)
    save_the_astronaut_sig = Value('b', False)
    changed_files_sig = Value('b', False)

    current_state = Value('i', 0)

    def __init__(self):
        """
        initiates the state manager with all necessary variables
        """
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "Initializing StateManager.")
        self.__running = True
        self.states = [
            IdleState(self),
            SafeState(self),
            SaveTheAstronaut(self),
            OperationState(self)
        ]
        self.__current_state = self.states[0]

        ## collect all threads/processes
        self.threads = []
        
        self.change_listener = ChangeListener(self)

        self.picom = PiCom()
        self.__read_process = Process(target=self.picom.receive_command, args=())
        self.__read_process.start()


        Logger.get_instance().log(LogLevel.INFO, "StateManager", "going in 5 seconds sleep before Main Loop.")
        time.sleep(5)

        try:
            if not os.path.exists(Main.swapp_folder + "logs/"):
                os.makedirs(Main.swapp_folder + "logs/")
            if not os.path.exists(Main.swapp_folder + "Database/"):
                os.makedirs(Main.swapp_folder + "Database/")
            if not os.path.exists(Main.swapp_folder + "videos/"):
                os.makedirs(Main.swapp_folder + "videos/")
            from helpers.data_copy_delete import DataCopyDelete           
            DataCopyDelete.get_instance().move_old_stuff_to_safe_folder()
            DataCopyDelete.get_instance().check_for_copystuff()
        except Exception as e:
            Logger.get_instance().log(LogLevel.EXCEPTION, "StateManager", str("copy logs/ to swapp folder did not work %s " %str(e)))

        # main loop
        while self.__running:
            try:
                self.__main_loop()
            except Exception as e:
                Logger.get_instance().log(LogLevel.ERROR, "StateManager", "Exception occured: " + str(e))
                time.sleep(1)

    def get_state(self):
        return StateManager.current_state.value

    def __reset(self):
        """
        resets the whole state manager to the initial state
        """
        Logger.get_instance().log(LogLevel.WARNING, "StateManager", "Reset of StateManager not yet supported.")


    def reinit(self):
        Logger.get_instance().__init__()
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "Reinitialize Database and Logger")




    def current_time(self):
        """
        :returns the current time in milliseconds
        """
        return int(round(time.time() * 1000))

    def change_state(self, new_state):
        """
        changes the current state
        :param new_state: the new state to run
        """
        self.__current_state = self.states[new_state]
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "Switching to state %s" % self.__current_state.name)
        StateManager.current_state.value = new_state
        #self.reinit()

    def __main_loop(self):
        """
        main loop of the state manager, running at given framerate
        """
        Logger.get_instance().log(LogLevel.INFO, "StateManager", "StateManager is entering main loop.")
        while self.__running:
            self.last_execution = self.current_time()
            # execution block
            self.__current_state.__run__()
            # end of execution block
            # checking execution time
            if self.current_time() - self.last_execution < 1 / StateManager.framerate * 1000:
                time.sleep(((1 / StateManager.framerate * 1000) - (self.current_time() - self.last_execution)) / 1000)
            else:
                Logger.get_instance().log(LogLevel.WARNING, "StateManager",
                                          "Can't reach target fps, current fps: %s" % (
                                              1 / (self.current_time() - self.last_execution) * 1000))
        self.__end()

    def stop_states(self):
        """
        function to stop the state manager, current execution will still finish
        """
        self.__running = False

    def __end(self):
        """
        end is executed after the main loop at the end of the state manager
        """
        Logger.get_instance().log(LogLevel.WARNING, "StateManager", "StateManager shutting down.")


class AbstractState:
    def __init__(self, state_manager):
        self._state_manager = state_manager
        self.name = "AbstractState"

    def __run__(self):
        if self._state_manager.save_the_astronaut_sig.value:
            self._state_manager.change_state(self._state_manager.SAVETHEASTRONAUT)
            return
        elif self._state_manager.safe_state_sig.value:
            self._state_manager.change_state(self._state_manager.SAFESTATE)
            return


class IdleState(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "IdleState"
        self.finished = Value('b', False)
        self.config_read_process = None


    def __run__(self):
        super().__run__()

    def __read_config(self):
        while (not self.finished.value) and (not self._state_manager.safe_state_sig.value) and \
                (not self._state_manager.save_the_astronaut_sig.value):
            while not self.__config_reader.check_for_config():
                time.sleep(0.1)

            # read in config
            try:
                self.__config_reader.parse_config()
                self.finished.value = True
            except Exception as e:
                self.__config_reader.update_version()
                Logger.get_instance().log(LogLevel.WARNING, "IdleState", "Could not parse Config. "
                                                                         "Exception occurred: "+str(e))
                Logger.get_instance().log(LogLevel.WARNING, "IdleState", "%s is ignored"
                                          % self.__config_reader.get_config_name())


class OperationState(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "OperationState"
        self.finished = Value('b', False)
        self.operation_process = None



    def __run__(self):
        super().__run__()
        if self.operation_process is None:

            # Setting up initial variables
            self.sensor_process = None

            """Flags"""
            self.__is_pumping = False
            self.__is_recording = False
            self.__video_counter = 1
            self.finished.value = False

    def __operations(self):

        self.finished.value = True


class SafeState(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "SafeState"
        self.recovered = Value('b', False)
        self.recover_process = None

    def __run__(self):
        if self.recover_process is None:
            self.recover_process = Process(target=self.__recover, args=())
            self.recover_process.start()
        if self.recovered.value:
            self.recovered.value = False
            self.recover_process = None
            Logger.get_instance().log(LogLevel.INFO, "SafeState", "The experiment was successfully recovered.")
            self._state_manager.change_state(StateManager.IDLESTATE)

    def __recover(self):
        Logger.get_instance().log(LogLevel.INFO, "SafeState", "The SafeState was triggered.")

        try:
            from camera.cameras import Camera
            cam = Camera()
            cam.stop_recording()
        except Exception as e:
            Logger.get_instance().log(LogLevel.WARNING, "SafeState", "Could not stop recording. Error: %s" % e)
        try:
            # 5. sleep for 30 minutes
            if not is_safe:
                Logger.get_instance().log(LogLevel.WARNING, "SafeState",
                                          "Could not recover. Trying again in 30 minutes.")
                time.sleep(60*30)
        except Exception as e:
            Logger.get_instance().log(LogLevel.WARNING, "SafeState", "Could not sleep. Error: %s" % e)

        self._state_manager.safe_state_sig.value = False
        self.recovered.value = True


class SaveTheAstronaut(AbstractState):
    def __init__(self, state_manager):
        super().__init__(state_manager)
        self.name = "SaveTheAstronautState"

    def __run__(self):
        Logger.get_instance().log(LogLevel.WARNING, "SaveTheAstronaut",
                                  "SaveTheAstronaut triggered, terminating all operations and shutting down.")
        print("shutting down system, testing only")
        #os._exit(0)
        os.system("/usr/bin/sudo /sbin/shutdown -h 0")
