##################################################
# PAPELL
# first created by Robin Schweigert
# modified by --
# successfully tested 04.02.2018 by Robin Schweigert
#
# This class is for all data handling between the
# Raspberry Pi and the ISS.
#
##################################################
from helpers.Singleton import Singleton
import os
from state_manager.Logger import Logger, LogLevel


@Singleton
class DataHandler:

    def __init__(self):
        # creating necessary directories:
        if not os.path.exists("logs/"):
            os.makedirs("logs/")
            Logger.get_instance().log(LogLevel.INFO, "DataHandler", "Created new log directory.")
        if not os.path.exists("logs/dataLists"):
            os.makedirs("logs/dataLists")
            Logger.get_instance().log(LogLevel.INFO, "DataHandler", "Created new data list directory.")

    def get_software_list(self):
        return DataHandler.get_instance().get_file_list(os.getcwd())

    def save_software_list(self):
        temp_software_file = open("logs/dataLists/sl_" + Logger.get_instance().get_current_time_str() + ".log", "a")
        temp_software_file.write(DataHandler.get_instance().get_software_list())
        temp_software_file.flush()
        Logger.get_instance().log(LogLevel.INFO, "DataHandler", "Created new software file list")
        temp_software_file.close()

    def get_log_list(self):
        return DataHandler.get_instance().get_file_list(os.getcwd() + "/logs")

    def save_log_list(self):
        temp_software_file = open("logs/dataLists/ll_" + Logger.get_instance().get_current_time_str() + ".log", "a")
        temp_software_file.write(DataHandler.get_instance().get_log_list())
        temp_software_file.flush()
        Logger.get_instance().log(LogLevel.INFO, "DataHandler", "Created new log file list")
        temp_software_file.close()

    def get_file_list(self, path):
        result = ""
        for dirname, dirnames, filenames in os.walk(path):
            for file in filenames:
                path = dirname + os.sep + file
                try:
                    result += path + ";" + str(os.path.getsize(path)) + "\n"
                except (FileNotFoundError, PermissionError):
                    result += path + ";-1\n"
        return result

    def save_file_list(self, path):
        temp_software_file = open("logs/dataLists/fl_" + Logger.get_instance().get_current_time_str() + ".log", "a")
        temp_software_file.write(DataHandler.get_instance().get_file_list(path))
        temp_software_file.flush()
        Logger.get_instance().log(LogLevel.INFO, "DataHandler", "Created new file list")
        temp_software_file.close()
