"""
created by Martin S on 20/07/18
"""
from multiprocessing import Process, Value, Queue
import time
import os
from helpers.Singleton import Singleton
from state_manager.Logger import Logger, LogLevel


@Singleton
class DataCopyDelete:

    def __init__(self):

        self.fileaddon = "/home/pi/papellobcsoftware/PapellObcSoftware/"
        self.__deletion_filename = self.fileaddon+"config/data_to_copy_and_delete.csv"  # ToDo: delete ../ from name
        self.__copy_deletion = None
        self.__move_files = None
        self.__safe_folder = self.fileaddon+"safe_folder_for_logDatasevideos/"
        self.__archive_folder =  str(self.fileaddon+ "archive/")
        self.__log_archive_folder = str(self.__archive_folder + "logs/")
        self.__db_archive_folder = str(self.__archive_folder + "databases/")
        self.__vid_archive_folder = str(self.__archive_folder + "videos/")


    def check_for_copystuff(self):
        """
        Checks for new Config files. Looks after beginning numbers (e.g. 02_Lin...) of config files.
        :return: Returns True if a new config file is available
        """

        # ToDo eventuell aus Loggerklasse (Logger.new_beginning)
        try:
            with open(self.__deletion_filename, 'r') as __file:
                self.__copy_deletion = __file.read().splitlines()
                self.parse_copy_stuff()
            return True
        except FileNotFoundError:
            Logger.get_instance().log(LogLevel.INFO, "Data Copy Deletion", "No file found")
            self.__copy_deletion = None
            return False
        except Exception as eeee:
            Logger.get_instance().log(LogLevel.WARNING, "Data Copy Deletion", "Problem due to %s" %(eeee))
            self.__copy_deletion = None
            return False
            pass # ToDo Could not load next config version

    def parse_copy_stuff(self):
        Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion", "copy process for "+self.__copy_deletion[0].replace("#", " ").replace(",", " "))
        #print(self.__copy_deletion[0].replace("#", " ").replace(",", " "))
        for line in self.__copy_deletion[1:]:  # each line in file
            __file = []
            component = line.split(",")
            __folder = component[0]
            for iii in range(1, len(component)):  # each filename in line
                __file.append(component[iii])
            for data  in __file:
                Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion", "copying "+self.fileaddon+__folder+"/"+data+" to USB drive")
                os.system("cp "+self.fileaddon+__folder+"/"+data+" /home/pi/mntData/"+__folder)
                #os.system("rsync -avrz "+self.fileaddon+__folder+"/"+data+" /home/pi/mntData/"+__folder)
        pass
        return True

    def move_old_stuff_to_safe_folder(self):
        move_stuff = False
        try:
            videos = []
            logs = []
            databases = []
            file = open("config/to_archiv", "r")
            for line in file:
                if line.__contains__(".log"):
                    move_stuff = True
                    logs.append(line)
                elif line.__contains__(".db"):
                    databases.append(line)
                    move_stuff = True
                elif line.__contains__(".h264"):
                    videos.append(line)
                    move_stuff = True
            Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion",
                                  str("Moving old logs (%s) files to archiv/logs folder of OBC. " % (str(logs))))
            Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion",
                                  str("Moving old logs (%s) files to archiv/databases folder of OBC. " % (str(databases))))
            Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion",
                                  str("Moving old logs (%s) files to archiv/videos folder of OBC. " % (str(videos))))
            file.close()
            move_stuff = True
        except Exception as e:
            Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion", str("Problem due to %s" % str(e)))
        if move_stuff:
            try:
                if not os.path.exists(self.__log_archive_folder):
                    os.makedirs(self.__log_archive_folder)
                if not os.path.exists(self.__db_archive_folder):
                    os.makedirs(self.__db_archive_folder)
                if not os.path.exists(self.__vid_archive_folder):
                        os.makedirs(self.__vid_archive_folder)
            except Exception as eeee:
                Logger.get_instance().log(LogLevel.WARNING, "Data Copy Deletion", "Problem due to %s" % (eeee))
                self.__copy_deletion = None
                return False
            try:
                self.origin_folder = "logs/"
                coplog = str()
                for a in logs:
                    coplog = coplog + str(a) + " "
                copdb = str()
                for a in databases:
                    copdb = copdb + str(a) + " "
                copvid = str()
                for a in videos:
                    copvid = copvid + str(a) + " "
                if coplog.__contains__("log"):
                    os.system("sudo mv  " + self.fileaddon + self.origin_folder + str(coplog) + self.__log_archive_folder)
                if copdb.__contains__("db"):
                    os.system("sudo mv  " + self.fileaddon + "Database/" + str(copdb) + self.__db_archive_folder)
                if copvid.__contains__("h264"):
                    os.system("sudo mv  " + self.fileaddon + "videos/" + str(coplog) + self.__vid_archive_folder)
                file = open("config/to_archiv", "w")
                file.write("")
                file.truncate()
                file.close()
                return True
            except Exception as eee:
                Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion", "Moving old files did not work with mv due to  "+str(eee))
                self.__copy_deletion = None
                return False
                pass  # ToDo Could not load next config version
        else:
            Logger.get_instance().log(LogLevel.INFO, "DataCopyDeletion", "Moving of old files is allready done. ")
            return True




if __name__ == '__main__':
    if 1:
        bla = DataCopyDelete()
        print("lol")
        bla.check_for_copystuff()
    if 0:
        print("DFECDE")
