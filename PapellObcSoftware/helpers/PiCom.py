##################################################
# PAPELL
# first created by Robin Schweigert
# modified by --
# successfully tested 10.02.2018 by Robin Schweigert
#
# This class implements the basic function for
# the inter pi communication.
#
##################################################

import serial
from multiprocessing import Process
from state_manager.Logger import Logger, LogLevel
from camera.cameras import Camera


class PiCom:

    # list of all commands
    PING = "p"  # starts new experiment
    STARTTRACKING = "st"
    STOPSTRACKING = "at"
    STARTVIDEO = "sv"
    STOPVIDEO = "av"
    GETFILES = "gf"
    SWAPDATA = "sd"
    COPYFILE = "cf"

    def __init__(self):
        """
        init the serial connection with baud 38400 and a timeout of 1s
        """
        self.__port = "/dev/serial0"
        self.__serial_con = serial.Serial(self.__port, 38400, timeout=1.0)
        self.__running = True
        Logger.get_instance().log("PiCom", LogLevel.INFO, "Created serial conection on port %s" % self.__port)
        #self.__read_process = Process(target=self.receive_command, args=())
        #self.__read_process.start()

        # for camera
        self.camera = Camera()
        self.__experiment_version = 0
        self.__video_counter = 1

    def send_command(self, command, arg=None):
        """
        sends a given command to including a \n via serial
        :param arg: argument for the given command
        :param command: the command to be send
        """
        try:
            if arg is None:
                msg = command + "\n"
                self.__serial_con.write(msg.encode("ASCII"))
            else:
                msg = command + ":" + arg + "\n"
                self.__serial_con.write(msg.encode("ASCII"))
        except Exception as e:
            print(e)


    def receive_command(self):
        """
        receives commands from the other pi
        """
        Logger.get_instance().log(LogLevel.INFO, "PiCom", "Started listening on port %s." % self.__port)
        while self.__running:
            try:
                temp_msg = self.__serial_con.readline()
                temp_msg = temp_msg.decode("ASCII")
                if temp_msg is not "":
                    if temp_msg.__contains__("p"):
                        self.__experiment_version = temp_msg[1:]
                        self.__video_counter = 1
                    elif temp_msg.__contains__("sv"):
                        self.start_camera(temp_msg)
                    elif temp_msg.__contains__("av"):
                        self.stop_camera(temp_msg)
                    Logger.get_instance().log(LogLevel.INFO, "PiCom", temp_msg)
            except Exception as e:
                Logger.get_instance().log(LogLevel.ERROR, "PiCom", str("Error: " + str(e)))

    def stop_serial(self):
        self.__running = False
        self.__serial_con.close()



    def start_camera(self, msg):
        filename = "videos/EA_2_Experiment_" + str(self.__experiment_version).zfill(2) \
                           + "_" + str(self.__video_counter).zfill(2) + ".h264"
        self.camera.start_recording(filename)

    def stop_camera(self, msg):
        self.camera.stop_recording()
        self.__video_counter += 1

if __name__ == '__main__':
    temp = PiCom()
    temp.send_command(temp.COPYFILE, "/dev/this/is/a/test")
