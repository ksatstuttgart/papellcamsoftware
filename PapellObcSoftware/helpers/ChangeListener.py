"""
created by robin on 29/07/18
"""

import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from multiprocessing import Process, Value
import subprocess
import os
from state_manager.Logger import  Logger, LogLevel


class ChangeListener(FileSystemEventHandler):

    # in s(!!)
    delay_time = 120 # testvalue of one minute should be 10 minutes or 20
    last_changed = Value('l', 2**31 - 1) ## pi is a 32 bit
    running = Value('b', True)

    def __init__(self,state_manager):
        super().__init__()
        self._state_manager = state_manager
        self.running.value = True
        self.last_changed = Value('l', 2**31 - 1)
        if "piData_1" in str(subprocess.check_output(['mount | grep piData'], shell=True)):
            self.path = "/piData_2/"
            self.logging("piData_2 is externally mounted")
        else:
            self.path = "/piData_1/"
            self.logging("piData_1 is externally mounted")
        self.observer = Observer()
        self.observer.schedule(self, self.path, recursive=True)
        self.observer.start()
        self.listen_process = Process(target=self.is_go, args=())
        self.listen_process.start()

    def on_any_event(self, event):
        super().on_any_event(event)
        self.last_changed.value = self.current_time_s()
        self.logging("set")
        self.logging(str(event))
        self.logging(self.last_changed.value)

    def current_time_s(self):
        return int(time.time())

    def reset_change(self):
        self.last_changed.value = 2**31 - 1
        self.logging("reset and ignore mounting difference")

    def is_go(self):
        time.sleep(60)
        self.reset_change()
        while self.running.value:
            if self.current_time_s() - self.last_changed.value > ChangeListener.delay_time:
                self.logging("go")
                self.logging(self.last_changed.value)
                self.swap()
                #self.reset_change()
            time.sleep(1)

    def stop(self):
        self.logging("stopped")
        self.running.value = False

    def logging(self, msg):
        Logger.get_instance().log(LogLevel.INFO, "ChangeListener", "%s ." % str(msg))


    def swap(self):
        self.logging("started swapping process")
        print(self._state_manager.get_state())
        self.stop()
        try:
            self.listen_process.terminate()
        except Exception as e:
            self.logging(str("could not terminate due to: %s" % str(e)))
        self.observer.stop()
        self.waiting = True
        try:
            while self.waiting:

                print(self._state_manager.get_state())
                if self._state_manager.get_state() != 3:
                    self.waiting = False
                    self.logging("copying first swapping then")
                    self._state_manager.reinit()
                    from helpers.data_copy_delete import DataCopyDelete
                    DataCopyDelete.get_instance().check_for_copystuff()
                    DataCopyDelete.get_instance().move_old_stuff_to_safe_folder()
                    self.logging("swapping now")
                    os.system("sudo swapData")
                    time.sleep(5)
                    if os.path.isfile("/home/pi/mntData/softwareupdate"):
                        os.system("sudo rm /home/pi/mntData/softwareupdate")
                        self.logging("softwareupdate found swapping back")
                        os.system("sudo swapData")
                        time.sleep(5)
                        os.system("sudo bash /home/pi/papellobcsoftware/PapellObcSoftware/reinit.sh")
                    elif os.path.isfile("/home/pi/mntData/configupdate") and os.path.isdir("/home/pi/mntData/configs"):
                        os.system("sudo rm /home/pi/mntData/configupdate")
                        self.logging("found new configs")
                        os.system("sudo cp /home/pi/mntData/configs/* /home/pi/papellobcsoftware/PapellObcSoftware/config/")
                        time.sleep(5)
                        self.__init__(self._state_manager)
                    else:
                        self.logging("no updates found carrying on")
                        self.__init__(self._state_manager)
                self.logging("waiting due to operations")
                time.sleep(10)
        except Exception as e:
            self.logging(str(e))
        #os.system("sudo swapData")
        #time.sleep(20)
        #self.__init__()


if __name__ == '__main__':
    temp = ChangeListener()
    input("enter to stop")
    temp.stop()
