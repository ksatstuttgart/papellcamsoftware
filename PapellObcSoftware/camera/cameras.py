##################################################
# PAPELL
# first created by Daniel Galla
# modified by --
# successfully tested --
#
# This is the assembly for all cameras and the
# lightning.
#
# libraries:
# sudo apt-get update
# sudo apt-get install python-picamera
##################################################

import picamera
from camera import  ivport
from state_manager.Logger import Logger, LogLevel


class Camera:

    # initializes camera with following parameters
    def __init__(self):
        try:
            self.camera = picamera.PiCamera()
            self.camera.sharpness = 0
            self.camera.contrast = 0
            self.camera.brightness = 50
            self.camera.saturation = 0
            self.camera.ISO = 0
            self.camera.video_stabilization = False
            self.camera.exposure_compensation = 0
            self.camera.exposure_mode = 'auto'
            self.camera.meter_mode = 'average'
            self.camera.awb_mode = 'auto'
            self.camera.image_effect = 'none'
            self.camera.color_effects = None
            self.camera.rotation = 0
            self.camera.hflip = False
            self.camera.vflip = False
            self.camera.crop = (0.0, 0.0, 1.0, 1.0)
            self.camera.resolution = (1296, 972)
            #self.camera.framerate = 5  # ToDo: for first ISS day
            self.camera.framerate = 15  # ToDo: for low resolution
            #self.camera.framerate = 24  # ToDo: for high resolution

            self.iv = ivport.IVPort(ivport.TYPE_QUAD2)

            Logger.get_instance().log(LogLevel.INFO, "Camera", " initialized.")
        except Exception as e:
            self.camera = None
            Logger.get_instance().log(LogLevel.ERROR, "Camera", str("Could not initialize Camera: " + str(e)))

    def start_recording(self, filename):
        """
        Starts recording with a given filename
        :param filename: Filename WITH path AND format (eg. videos/test.h264)
        :return:
        """
        try:
            self.iv.camera_change(1)
            self.camera.start_recording(filename)
            Logger.get_instance().log(LogLevel.INFO, "Camera", "Started taking Video [%s]." % filename)
        except Exception as e:
            Logger.get_instance().log(LogLevel.ERROR, "Camera", str("Could not start Camera: " + str(e)))

    def stop_recording(self):
        """
        Stops recording
        :return:
        """
        try:
            self.camera.stop_recording()
            Logger.get_instance().log(LogLevel.INFO, "Camera", "Started taking a Video.")
        except:
            Logger.get_instance().log(LogLevel.ERROR, "Camera", "Could not stop Camera.")

