##################################################
# PAPELL
# first created by
# modified by Martin Siedorf
# successfully tested --
#
# This is the assembly for the
# lightning.
#
# libraries:
#
##################################################

from helpers.LP55231 import LP55231
from state_manager.Logger import Logger, LogLevel
from state_manager.initialcheck import i2c_action


class Lightning:

    ON = 1
    OFF = 0

    __num_of_leds = 0

    def __init__(self, num_of_leds):
        """
        Sets the specified pins as output pins for the LEDs
        :param num_of_leds: Number of connected LEDs
        """

        self.__num_of_leds = num_of_leds
        self.led_chip = LP55231(i2c_action.Addr_LED)
        Logger.get_instance().log(LogLevel.INFO, "Lightning", " initialized")

    def on(self):
        """
        Turns all the LEDs on
        :return:
        """
        for pin in range(0, self.__num_of_leds):
            self.led_chip.SetChannelPWM(pin, 255)
        Logger.get_instance().log(LogLevel.INFO, "Lightning", "LED lightning is on.")

    def off(self):
        """
        Turns all the LEDs off
        :return:
        """
        for pin in range(0, self.__num_of_leds):
            self.led_chip.SetChannelPWM(pin, 0)
        Logger.get_instance().log(LogLevel.INFO, "Lightning", "LED lightning is off.")

# ----------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------unused functions------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

    def set(self, *pins, state=None, state_list=None):
        """
        EITHER sets all LEDs after a entered list of LED states
            Example: set(state_list=[0,1,0,0,1]) turns LED 2 and 5 of 5 LEDs on and turns the rest off
        OR sets specified pins on or off
            Example: set(1, 4, 6, state=Lightning.ON) turns LEDs 1, 4 and 6 on
        :param state_list:  List of states of the LEDs (0=Off, 1=On)
        :param pins:        Pins to be turned on/off
        :param state:       State, the LEDs shall be switched to (Lightning.ON / Lightning.OFF)
        :return:
        """
        if state_list is None:
            for pin in pins:
                self.set_pin(pin, state)
        else:
            if state_list.size == self.__num_of_leds:
                for pin in range(0, self.__num_of_leds):
                    self.set_pin(pin, state_list[pin])

    def set_pin(self, pin, state):
        """
        Sets one pin ON or OFF and checks if the entered pin is in __list_of_pins
        :param pin:
        :param state: Lightning.ON / Lightning.OFF to turn the LED on / off
        :return:
        """
        if pin < self.__num_of_leds:
            try:
                if state is self.ON:
                    self.led_chip.SetChannelPWM(pin, 255)
                elif state is self.OFF:
                    self.led_chip.SetChannelPWM(pin, 0)
                else:
                    Logger.get_instance().log(LogLevel.WARNING, "Lightning",
                                              "No state (ON/OFF) for LED on pin number %s entered." % pin)
            except ValueError:
                Logger.get_instance().log(LogLevel.WARNING, "Lightning",
                                          "Could not switch on/off a lightning LED. "
                                          "Pin number %s does not exist." % pin)
            except TypeError:
                Logger.get_instance().log(LogLevel.WARNING, "Lightning",
                                          "Could not switch on/off a lightning LED. "
                                          "Pin number %s is not an Integer." % pin)
            except:
                Logger.get_instance().log(LogLevel.WARNING, "Lightning",
                                          "Could not switch on/off lightning LED on pin %s." % pin)
        else:
            Logger.get_instance().log(LogLevel.WARNING, "Lightning",
                                      "Cannot turn on/off pin %s of %s pins." % (pin, self.__num_of_leds))
