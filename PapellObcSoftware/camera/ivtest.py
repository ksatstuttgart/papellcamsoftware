import ivport2 as ivport
import os


# raspistill capture
def capture(camera, i):
    "This system command for raspistill capture"
    cmd = "raspistill -t 1 -o pics/still_CAM%d_%s.jpg" % (camera, i)
    os.system(cmd)

iv = ivport.IVPort(ivport.TYPE_QUAD2)
for i in range(0, 4):
    print(i)
    print("Cam 1")
    input("")
    iv.camera_change(1)
    capture(1, i)
    print("Cam 2")
    input("")
    iv.camera_change(2)
    capture(2, i)
    print("Cam 3")
    input("")
    iv.camera_change(3)
    capture(3, i)
    print("Cam 4")
    input("")
    iv.camera_change(2)
    capture(4,1)

print("close")
iv.close()
